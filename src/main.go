package main

import (
	"context"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
)

type AwardEmojiJson struct {
	Name  string
	Emoji string
	Count int
}

var ctx = context.Background()

var slackEmoji = getEnv("SLACK_EMOJI", ":thumbsup:")

func main() {

	var k AwardEmojiJson

	gitlabToken := getEnv("GITLAB_TOKEN", "")
	gitlabAwardType := getEnv("GITLAB_AWARD_TYPE", "thumbsup")
	gitlabAddr := getEnv("GITLAB_ADDR", "https://gitlab.com/api/v4")
	gitlabMrState := getEnv("GITLAB_MERGE_REQUEST_STATE", "opened")
	gitlabGroup := getEnv("GITLAB_GROUP_ID", "")

	slackApiToken := getEnv("SLACK_TOKEN", "")
	slackChannelId := getEnv("SLACK_CHANNEL_ID", "")

	redisHost := getEnv("REDIS_HOST", "localhost:6379")

	notificationTextsPath := getEnv("NOTIFICATION_TEXT_FILE_PATH", "notifications.txt")
	notificationTexts := getNotificationTexts(notificationTextsPath)

	git, _ := gitlab.NewClient(gitlabToken, gitlab.WithBaseURL(gitlabAddr))

	redisDb := redis.NewClient(&redis.Options{
		Addr: redisHost,
	})

	slackApi := slack.New(slackApiToken)

	log.Printf("Starting..")

	for {

		projects, _, _ := git.Groups.ListGroupProjects(gitlabGroup, nil)

		mergeRequestOpts := &gitlab.ListProjectMergeRequestsOptions{
			State: gitlab.String(gitlabMrState),
		}

		for _, projects := range projects {

			mergeRequests, _, _ := git.MergeRequests.ListProjectMergeRequests(projects.ID, mergeRequestOpts)

			log.Printf("Geting all award emoji per MRs..")

			for _, mr := range mergeRequests {

				aes, _, _ := git.AwardEmoji.ListMergeRequestAwardEmoji(projects.ID, mr.IID, nil)

				log.Printf("Counting how many '%s' has MR", gitlabAwardType)

				awardCount := 0

				for _, award := range aes {
					if award.Name == gitlabAwardType {
						awardCount++
						log.Printf("MR: '%v' has %v awards", mr.Title, awardCount)
					}
				}

				mrUid := projects.ID + mr.IID
				mrKey := b64.StdEncoding.EncodeToString([]byte(strconv.Itoa(mrUid)))

				m := AwardEmojiJson{mr.Title, gitlabAwardType, awardCount}

				mrAwardStatus, _ := json.Marshal(m)

				n := rand.Intn(len(notificationTexts))

				switch result, err := redisDb.Get(ctx, mrKey).Result(); {
				case err == redis.Nil:
					log.Printf("Award state does not exist. Creating..")

					e := redisDb.Set(ctx, mrKey, mrAwardStatus, 0).Err()
					if e != nil {
						panic(e)
					}
					if awardCount > 0 {
						sendToSlack(slackApi, slackChannelId, mr, awardCount, notificationTexts[n])
					}
				case err != redis.Nil:
					log.Printf("Award state found. Checking..")

					err := json.Unmarshal([]byte(result), &k)

					if err != nil {
						log.Printf("could not unmarshal json: %s\n", err)
						return
					}

					if awardCount > k.Count {
						e := redisDb.Set(ctx, mrKey, mrAwardStatus, 0).Err()
						if e != nil {
							panic(e)
						}

						sendToSlack(slackApi, slackChannelId, mr, awardCount, notificationTexts[n])
					} else {
						log.Printf("Nothing to do. Skipping for MR: %s", mr.Title)
					}
				case err != nil:
					panic(err)

				}

			}

		}
		log.Printf("Wait for next iteration..")
		time.Sleep(60 * time.Second)
	}

}

func sendToSlack(sk *slack.Client, channel string, mr *gitlab.MergeRequest, count int, notificationText string) {
	text := fmt.Sprintf("%d x %s <%s|%s>. %s ", count, slackEmoji, mr.WebURL, mr.Title, notificationText)
	channelID, _, err := sk.PostMessage(
		channel,
		slack.MsgOptionText(text, false),
		slack.MsgOptionDisableLinkUnfurl(),
	)
	if err != nil {
		log.Printf("%s\n", err)
		return
	}
	log.Printf("Notification successfully sent to channel %s.", channelID)
}

func getNotificationTexts(filePath string) []string {
	fileBytes, err := ioutil.ReadFile(filePath)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	sliceData := strings.Split(string(fileBytes), "\n")

	return sliceData
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
