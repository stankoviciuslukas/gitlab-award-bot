FROM golang:1.19-alpine AS base

WORKDIR /app

COPY src ./

RUN go mod tidy

RUN go build -o gitlab-award-bot

FROM alpine:3.17

WORKDIR /app

COPY --from=base /app/gitlab-award-bot .

CMD ["/gitlab-award-bot"]
