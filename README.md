## Gitlab Award Bot

A simple bot/script that tracks all MRs in all projects that are available to specific group id.

And sends notification to specified Slack channel with current award status and a notification text.

### ENVs

| Name                        | Description                                                                  | Default                   |
|-----------------------------|------------------------------------------------------------------------------|---------------------------|
| GITLAB_TOKEN                | Access Token for accessing GitLab API. Must have read_api permissions        | ""                        |
| GITLAB_MERGE_REQUEST_STATE  | Merge request state to follow                                                | opened                    |
| GITLAB_AWARD_TYPE           | Merge request award type to follow                                           | thumbsup                  |
| GITLAB_ADDR                 | Gitlab API address                                                           | https://gitlab.com/api/v4 |
| GITLAB_GROUP_ID             | Gitlab Group ID to track merge requests                                      | ""                        |
| SLACK_TOKEN                 | Slack Token for posting notification about current merge request award state | ""                        |
| SLACK_CHANNEL_ID            | Slack Channel ID to post to                                                  | ""                        |
| SLACK_EMOJI                 | Slack Emoji to add in notification text                                      | :thumbsup:                |
| REDIS_HOST                  | Redis host for award state                                                   | localhost:6379            |
| NOTIFICATION_TEXT_FILE_PATH | Slack notification text file                                                 | notifications.txt         |

### TODO

- Load from .env file
- Multiple groups to track
